#!/bin/bash
# Use this script to combine all *.csv files in a directory,
# creating a new file called combined.csv
#
# 2020-03-13 plusk@mit.edu

DIR=$1

if [ -z "$DIR" ]; then
    DIR=.
fi

# backup any existing combined.csv
if [[ -f "./combined.csv" ]]; then
    mv "./combined.csv" "./combined.csv.bak"
fi

# find all existing csvs
FILES=()
while IFS= read -r -d $'\0'; do
    FILES+=("$REPLY")
done < <(find $DIR -name "*.csv" -print0)

# copy first line from one of the existing csv files
for i in ${!FILES[@]}; do
    file=${FILES[$i]}
    if [ $i == 1 ]; then
        head -n 1 "$file" > "./combined.csv"
    fi

    # ask user if we should add this file
    read -r -p "Use $file? [Y/n] " response
    response=${response,,} # tolower
    if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
        # combine into one
        tail -n +2 "$file" >> "./combined.csv"
    fi
done

