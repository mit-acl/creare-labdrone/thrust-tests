# Gabriel Bousquet, g_b@mit.edu 02/23/2018
# MIT ACL


# The script performs a fit to proproller thrust data to generate a thrust curve
# The data is to be generated with the RCbenchmark propeller test bench.
# This may be done with the propeller_charaterization.js script, provided.
# The thrust curve typically depends on the battery-ESC-motor-propeller system.

# The input to the ESC is a PWM signal between 1000 and 2000 us.
# The main output is the propeller thrust.
# Another output of interest is the drag factor i.e.the proportionality factor between the propeller thrust and the torque applied to the propeller.

# The aim of the script is to give a fit to the two following functions:
# * direct thrust curve:
#   PWM (us) |--> Thrust (N)
#   This mapping is modeled in the form  Thrust = a*PWM^2 + b*PWM + c
# * reciptocal thrust curve (used for control: one wants a given force and wants to know what PWM signal to send to the ESC):
#   Thrust (N) |--> PWM (us)
#   This mapping is modeled in the form PWM = A + sqrt(B + C*Thrust)

# Usage: load a csv file containing the propeller thrust data in the followind line
input_file = "prop_data_concatenated.csv"
# The script returns direct and reciprocal fits i.e. a, b, c and A, B, C.
# The script also plots the fits, as well as the propeller drag factor.
# AT the PWMs that we have been operating at, the drag factor is fairly constant
# and the fit for the drag factor was eyeballed from the plot.

# Remarks:
# * The thrust curve depends on the voltage. The script has hardcoded the voltage range on which to do the fit (here: 15.25 to 15.75V, which is for a 4s LiPo).
# * The Thrust typically saturated for large PWMs. Here, we restrict the fit to PWM values below 1900 us. (90% throttle).






import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


plt.ion()
fig1 = plt.figure(1)
fig1.clf()
ax1 = fig1.add_subplot(111)
g = 9.81

fig2 = plt.figure(2)
fig2.clf()
ax2 = fig2.add_subplot(111)


propdata = pd.read_csv(input_file)
propdata.rename(columns={'ESC signal (µs)':'ESC signal (us)'}, inplace=True)

dataScatter = ax2.scatter(propdata['ESC signal (us)'], propdata['Thrust (kgf)'], c = propdata['Voltage (V)'])
cbar = fig2.colorbar(dataScatter)
cbar.set_label('Battery Voltage (V)')
ax2.set_xlabel('ESC PWM (us)')
ax2.set_ylabel('Thrust (kgf)')
ax2.set_title('Motor Thrust Curve')

ax1.scatter(propdata['ESC signal (us)'], propdata['Torque (N·m)']/(propdata['Thrust (kgf)']*g), c = propdata['Voltage (V)'])
ax1.set_ylim(.01, .02)
ax1.set_xlabel('Torque (N.m)')
ax1.set_ylabel('Propeller drag factor (m)')



### Fit of the pwm-thrust curve

quad = lambda x, a, b, c,: a*x**2 + b*x + c

max_pwm_for_fit = 1900

pwmPercent = (propdata[propdata['ESC signal (us)']<max_pwm_for_fit]['ESC signal (us)'] - 1000)/1000.
thrust = propdata[propdata['ESC signal (us)']<max_pwm_for_fit]['Thrust (kgf)']*g
typ_voltage = (propdata['Voltage (V)']<15.75) & (propdata['Voltage (V)']>15.25) & (propdata['ESC signal (us)']<max_pwm_for_fit)
thrust_typ_voltage = propdata[typ_voltage]['Thrust (kgf)']*g
pwmPercent_typ_voltage = (propdata[typ_voltage]['ESC signal (us)'] - 1000)/1000.

sol = curve_fit(quad, pwmPercent, thrust)
solpwmtoF = curve_fit(quad, pwmPercent, thrust)

percents = np.linspace(0, 1, 100)
pwmpoints = 1000.*(percents + 1.)
c1, = ax2.plot(pwmpoints,  quad(percents,*sol[0])/g)
print  "pwm to F fit",  sol[0]

SQRT = lambda y, A, B, C: A + np.sqrt(B + C*y)
solconv = curve_fit(SQRT, thrust, pwmPercent)
# solconv = curve_fit(SQRT, thrust_typ_voltage, pwmPercent_typ_voltage)
thrustpoints = np.linspace(0, 1.2*g, 100)
c2, = ax2.plot((SQRT(thrustpoints, *solconv[0]) + 1.)*1000., thrustpoints/g)
print  "F to pwm fit",  solconv[0]
ax2.legend([c1, c2], ['direct fit (quadratic)', r'reciprocal fit $x = a + \sqrt{b + cy}$'])
