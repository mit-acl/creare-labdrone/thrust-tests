# Gabriel Bousquet, g_b@mit.edu 02/23/2018
# MIT ACL

# The script performs a fit to the dynamic response of the thrust, i.e.
# the response of the thrust to a step input in PWM command.
# The data is to be generated with the RCbenchmark propeller test bench.
# This may be done with the measure_settling_time.js script, provided.

# The dynamic response typically depends on the battery-ESC-motor-propeller combination,
# as well as on the step amplitude and whether the step is positive or negative.
# with our setup, the dependence on step amplitude or direction seemed to be small.

# The step response is modeled as a first order response with delay.

# Important remark: there is a delay in communication between the PWM input and the RCbenchmark logging system. Accordingly, the delay in the data is likely not a physical delay, but rather it is due to the acquisition system.


# Usage: load a csv file containing the propeller dynamic response data in the following line
input_file = dyn1.csv
# the system plots an overlay of all the step responses and displays the data's lag and pole of the dynamic thrust response.





import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from ipdb import set_trace as st

g = 9.81


plt.ion()
fig1 = plt.figure(1)
fig1.clf()
ax11 = fig1.add_subplot(211)
ax12 = fig1.add_subplot(212, sharex = ax11)

fig2 = plt.figure(2)
fig2.clf()
ax2 = fig2.add_subplot(111)

data1 = pd.read_csv(input_file)
data1.rename(columns={'ESC signal (µs)':'ESC signal (us)'}, inplace=True)

ax11.plot(data1['Time (s)'], data1['Motor Electrical Speed (RPM)'], '+')
ax12.plot(data1['Time (s)'], data1['ESC signal (us)'], '+')

ax11.set_ylim(6000, 10000)
ax11.set_ylabel('motor rpm')
ax12.set_ylabel('esc pwm (us)')
ax12.set_xlabel('t (s)')
ax12.set_ylim(1500, 1800)
fig1.suptitle('Time series for dynamic response fit')


pwm = data1['ESC signal (us)']

cmdchge = pwm != pwm.shift()
cmdchge = cmdchge.loc[cmdchge].index

split = [data1[i:j] for i, j in zip(cmdchge[:-1], cmdchge[1:])]


fitfun = lambda t, dt, tau: (1 - np.exp(- (t - dt)/tau))*(t>dt)
shift = 0.

sol = []
for stepi in split[2::2]:
    #remove outliers

    stepiy = stepi['Motor Electrical Speed (RPM)']
    # asymptote. Take the 1/2 largest values, take the mean
    asympt = np.mean(stepiy[stepiy>stepiy.quantile(.5)])
    t = stepi['Time (s)']
    t -= t.iloc[0]
    y =  (stepiy - stepiy.iloc[0])/(asympt - stepiy.iloc[0])

    y_for_fit = y[(t>.05)*(t <.4)]
    t_for_fit = t[(t>.05)*(t <.4)]

    soli = curve_fit(fitfun, t_for_fit, y_for_fit, [.05, .1])
    print soli[0]
    sol.append(soli[0])
    dt = soli[0][0]
    line, = ax2.plot(t - dt, y + shift , '+')
    ax2.plot(t - dt, fitfun(t, *soli[0]) + shift, color = line.get_color())


    #shift += .1


for stepi in split[3::2]:
    #remove outliers

    stepiy = -stepi['Motor Electrical Speed (RPM)']
    # asymptote. Take the 1/2 largest values, take the mean
    asympt = np.mean(stepiy[stepiy>stepiy.quantile(.5)])
    t = stepi['Time (s)']
    t -= t.iloc[0]
    y =  (stepiy - stepiy.iloc[0])/(asympt - stepiy.iloc[0])

    y_for_fit = y[(t>.05)*(t <.4)]
    t_for_fit = t[(t>.05)*(t <.4)]

    soli = curve_fit(fitfun, t_for_fit, y_for_fit, [.05, .1])
    print soli[0]
    sol.append(soli[0])
    dt = soli[0][0]
    line, = ax2.plot(t - dt, y + shift , '+')
    ax2.plot(t - dt, fitfun(t, *soli[0]) + shift, color = line.get_color())


    #shift += .1

mean_delay = np.mean([soli[0] for soli in sol])
mean_tau = np.mean([soli[1] for soli in sol])
print "delay mean, std:", mean_delay, np.std([soli[0] for soli in sol]),""
print "tau mean, std:", mean_tau, np.std([soli[1] for soli in sol])


ax2.set_xlabel('t (s)')
ax2.set_ylabel('rpm response, renormalized')
ax2.set_title('RPMx step response. Delay: %3fs, tau: %.3fs'%(mean_delay, mean_tau))
ax2.set_xlim(-.1, .4)
ax2.set_ylim(-.1, 1.2)

