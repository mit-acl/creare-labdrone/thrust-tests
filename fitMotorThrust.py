#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script fits the thrust curve (Newton vs percentage_ESC) to the data produced by the testbed https://www.rcbenchmark.com/
It first finds force_in_Newtons=a*(percent_esc)**2 + b*percent_esc + c
and then finds percent_esc = A + sqrt(B + C*force_in_Newtons)

Modified somewhat from MIT code by Gabriel. From "thrust_curve.py"

2019-Nov-18--> Modified by Jesus Tordesillas Torres, jtorde@mit.edu
2019-10-01 @jkp 
2020-03-12 plusk@mit.edu

"""

import os
import argparse

import numpy as np; np.set_printoptions(linewidth=200)
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

GRAVITY = 9.81

# PWM range of interest. Should be the same as used in the esc_interface (see esc_interface.h).
# This really shouldn't be different than 1000-2000.
MAX_PWM_USEC = 2000
MIN_PWM_USEC = 1000

def process_csv(filename, min_pwm_us=1000, max_pwm_us=2000):

    # Rename microseconds \mu to u, and N \cdot m to N.m
    propdata = pd.read_csv(filename, sep=',')
    colname_replacements = {}
    for col in propdata.columns:
        if '\xb5' in col: # 'µ'
            colname_replacements[col] = col.replace('\xb5','u')
        if '\xb7' in col: # '·'
            colname_replacements[col] = col.replace('\xb7','.')

    propdata.rename(columns=colname_replacements, inplace=True)

    # only use data that is within the pwm values we are interested in
    propdata = propdata[ (propdata['ESC signal (us)'] <= MAX_PWM_USEC) & (propdata['ESC signal (us)'] >= MIN_PWM_USEC) ]

    pwm_us = propdata['ESC signal (us)']
    torqueNm = propdata['Torque (N.m)']
    thrustkgf = propdata['Thrust (kgf)']
    voltage = propdata['Voltage (V)']

    return pwm_us, torqueNm, thrustkgf, voltage, propdata

def poly(n):
    """
    Builds an nth order polynomial

        >>> p = poly(2)
        >>> p(x,a,b,c) == a*x**2 + b*x + c

    """

    # gross hacky way
    if n == 1: return lambda x, a, b: a*x + b
    if n == 2: return lambda x, a, b, c: a*x**2 + b*x + c
    if n == 3: return lambda x, a, b, c, d: a*x**3 + b*x**2 + c*x + d
    if n == 4: return lambda x, a, b, c, d, e: a*x**4 + b*x**3 + c*x**2 + d*x + e
    if n == 5: return lambda x, a, b, c, d, e, f: a*x**5 + b*x**4 + c*x**3 + d*x**2 + e*x + f
    if n == 6: return lambda x, a, b, c, d, e, f, g: a*x**6 + b*x**5 + c*x**4 + d*x**3 + e*x**2 + f*x + g

    ## The following doesn't work since curve_fit needs to be able to count args
    # def poly_h(n, x, *args):
    #     if n == 0:
    #         return args[0]
    #     return args[0]*x**n + poly_h(n-1, x, *args[1:])

    # nn = n
    # return lambda *args: poly_h(nn, *args)


if __name__ == '__main__':
  
    parser = argparse.ArgumentParser(description="Create thrust-pwm model from RCBenchmark csv file")
    parser.add_argument('file', type=str, help="csv file")
    parser.add_argument('-o', '--fit-order', type=int, help="order of polynomial fit", default=2)
    args = parser.parse_args()

    #
    # Load the data
    #

    pwmus, torqueNm, thrustkgf, voltage, propdata = process_csv(args.file, 
                                                    min_pwm_us=MAX_PWM_USEC,
                                                    max_pwm_us=MIN_PWM_USEC)


    # data preprocessing
    thrustN = thrustkgf * GRAVITY
    pwmPercent = (pwmus - MIN_PWM_USEC) / (MAX_PWM_USEC - MIN_PWM_USEC)

    #
    # Plot initial data
    #

    # make sure grid lines are below any data points / lines
    plt.rc('axes', axisbelow=True)

    plt.figure(); plt.grid()
    dataScatter = plt.scatter(pwmus, torqueNm/(thrustkgf*GRAVITY), c=voltage)
    cbar = plt.colorbar(dataScatter); cbar.set_label('Battery Voltage (V)')
    plt.xlabel('ESC PWM (us)'); plt.ylabel('Propeller drag factor (m)')

    # plot thrust vs pwm
    plt.figure(); plt.grid()
    dataScatter = plt.scatter(pwmus, thrustkgf, c=voltage)
    cbar = plt.colorbar(dataScatter); cbar.set_label('Battery Voltage (V)')
    plt.xlabel('ESC PWM (us)'); plt.ylabel('Thrust (kgf)')
    plt.title('Motor Thrust Curve')

    #
    # Analysis - pwm to thrust
    #

    P = poly(args.fit_order) # 2nd order (quadratic) polynomial
    popt, _ = curve_fit(P, pwmPercent, thrustN)

    # plot thrust vs pwm
    plt.figure(); plt.grid()
    dataScatter = plt.scatter(pwmPercent, thrustN, c=voltage)
    cbar = plt.colorbar(dataScatter); cbar.set_label('Battery Voltage (V)')
    plt.xlabel('ESC PWM (%)'); plt.ylabel('Thrust (N)')
    plt.title('Motor Thrust Curve')

    u = np.linspace(0, 1, 100)
    plt.plot(u, P(u,*popt))
    plt.legend(['polynomial fit'])

    print("Polynomial({}): Thrust (N) as a function of PWM (%) parameters: {}".format(args.fit_order, popt))

    #
    # Analysis - thrust to pwm
    #

    P = poly(args.fit_order)
    popt, _ = curve_fit(P, thrustN, pwmPercent)

    # plot thrust vs pwm
    plt.figure(); plt.grid()
    dataScatter = plt.scatter(thrustN, pwmPercent, c=voltage)
    cbar = plt.colorbar(dataScatter); cbar.set_label('Battery Voltage (V)')
    plt.xlabel('Thrust (N)'); plt.ylabel('ESC PWM (%)')
    plt.title('Motor Thrust Curve')

    T = np.linspace(0, max(thrustN), 100)
    plt.plot(T, P(T,*popt))

    print("Polynomial({}): PWM (%) as a function of Thrust (N) parameters: {}".format(args.fit_order, popt))

    # square root curve (inverse of 2nd order polynomial)
    SQRT = lambda y, A, B, C, D: A + np.sqrt(B + C*y + D*y*y)
    bounds = ([-0.5, 0.,0.,0.], [0.,0.1,0.5,0.5])
    popt, _ = curve_fit(SQRT, thrustN, pwmPercent, method='trf', bounds=(bounds))

    plt.plot(T, SQRT(T,*popt), label='sqrt')
    plt.legend(['polynomial fit', r'square root fit $x = a + \sqrt{b + cy + dy^2}$'])

    print("sqrt: PWM (%) as a function of Thrust (N) parameters: {}".format(popt))



    plt.show()
