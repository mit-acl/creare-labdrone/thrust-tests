Thrust Stand Data
=================

## FAQ

1. `fitMotorThrust.py`: square root curve gives error about non-finite residuals. Make sure that all thrust values are positive. If they are not, you may need to manually flip the signs. If you forgot to tare and the first few samples are negative, you may need to manually zero the thrust by adding that first value to the rest (hint: use copy and paste, LibreOffice Calc, and MATLAB).
